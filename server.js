const express = require('express');
const multer = require('multer');
const fs = require('fs-extra');
const path = require('path');
const axios = require('axios');
const jwt = require('jsonwebtoken');
const jwktopem = require("jwk-to-pem");

// Load environment variables
require('dotenv').config();

const app = express();
const upload = multer({ dest: 'uploads/' });

// Use environment variables with defaults
const PORT = process.env.SERVER_PORT || 3000;
const REQUIRE_AUTH = process.env.REQUIRE_AUTH === 'true';
const OAUTH_URI = process.env.OAUTH_URI; // Discovery URI (`.well-known/openid-configuration`)

// Middleware to check the Authorization Bearer token and verify the JWT
const checkToken = async (req, res, next) => {
    if (REQUIRE_AUTH) {
        const { headers } = req;
        const authorizationHeader = headers.authorization;
        
        // Check if the Authorization header exists and starts with "Bearer "
        if (!authorizationHeader || !authorizationHeader.startsWith('Bearer ')) {
            res.status(401).json({ error: 'Unauthorized: Invalid or missing token' });
            return;
        }

        // Retrieve the JWKS_URI from the Discovery URI
        try {
            const { data } = await axios.get(OAUTH_URI);
            jwksUri = data.jwks_uri; // This is the URI where the JWKS can be retrieved
        } catch (error) {
            res.status(401).json({ error: 'Unauthorized: Error fetching OIDC document' });
            return;
        }

        // Extract the token from the header
        const token = authorizationHeader.substring(7);

        try {
    
            // Retrieve the JWKS from the JWKS URI
            const response = await axios.get(jwksUri);
            const jwks = response.data;

            // Extract the JWT kid (key ID) from the header
            const { header } = jwt.decode(token, { complete: true });
            const kid = header && header.kid;

            // Find the appropriate key in the JWKS based on the kid
            const key = jwks.keys.find((key) => key.kid === kid);
            if (!key) {
                res.status(401).json({ error: 'Unauthorized: Invalid key id' });
                return
            }

            // Verify the JWT token using the retrieved public key
            const decodedToken = jwt.verify(token, jwktopem(key), {algorithms: [key.alg]});
            const { exp, iat } = decodedToken;

            // Verify the expiration time (exp)
            const currentTimestamp = Math.floor(Date.now() / 1000);
            if (exp && exp < currentTimestamp) {
                res.status(401).json({ error: 'Unauthorized: Expired Token' });
                return;
            }

            // Verify the issued at time (iat)
            if (iat && iat > currentTimestamp) {
                res.status(401).json({ error: `Unauthorized: Time-travelling Token ${iat} > ${currentTimestamp}` });
                return;
            }

            // The decoded token can be accessed in subsequent route handlers if necessary
            req.decodedToken = decodedToken;

            next();
            return
        } catch (error) {
            res.status(401).json({ error: 'Unauthorized: Invalid token' });
            return;
        }
    }
    next();
};

app.use(express.json());

// Directory where files will be stored
const filesDirectory = path.join(__dirname, 'uploads');
fs.ensureDir(filesDirectory);

// File to store updates information
const updatesFile = path.join(__dirname, 'updates.json');

// Ensure updates.json exists and has valid content
fs.ensureFile(updatesFile)
  .then(() => fs.readJson(updatesFile))
  .then((data) => {
    if (!data || Object.keys(data).length === 0) {
      // File is empty or contains invalid JSON, initialize it with an empty object
      return fs.writeJson(updatesFile, {}, { spaces: 4, EOL: '\n' });
    }
    // File already has content, no need to overwrite
  })
  .catch((error) => {
    if (error.code === 'ENOENT' || error instanceof SyntaxError) {
      // File does not exist or contains invalid JSON, initialize it with an empty object
      return fs.writeJson(updatesFile, {}, { spaces: 4, EOL: '\n' });
    }
    // Handle other potential errors
    console.error("Error ensuring updates file:", error);
  });

// PUT /commit/{project_key}/{hash}
app.put('/commit/:project_key/:hash', checkToken, upload.single('file'), async (req, res) => {
    if (!req.file) {
        return res.status(400).send('No file uploaded.');
    }

    const { project_key, hash } = req.params;
    const filePath = path.join(filesDirectory, req.file.filename);

    // Read current state from updates.json
    const updates = await fs.readJson(updatesFile);

    // Check if there is an existing file for this project_key
    if (updates[project_key] && updates[project_key].file) {
        // Remove existing file
        const existingFilePath = path.join(filesDirectory, updates[project_key].file);
        await fs.unlink(existingFilePath);
    }
    
    // Update file information in updates.json
    updates[project_key] = { hash, file: req.file.filename };
    await fs.writeJson(updatesFile, updates, { spaces: 4, EOL: '\n' });

    console.log(`COMMIT ${project_key}: ${hash} (${req.file.filename})`)
    res.send(`File for ${project_key} uploaded successfully.`);
});

// GET /check/{project_key}
app.get('/check/:project_key', async (req, res) => {
    const { project_key } = req.params;
    const updates = await fs.readJson(updatesFile);
    
    if (updates[project_key]) {
        console.log(`CHECK '${project_key}': ${updates[project_key].hash}`)
        res.send(updates[project_key].hash);
    } else {
        console.error(`CHECK '${project_key}': 400`)
        res.status(400).send('project_key does not exist.');
    }
});

// GET /check
app.get('/check', async (req, res) => {
    const updates = await fs.readJson(updatesFile);
    console.log(`CHECK`)
    res.json(updates);
});

// GET /fetch/{project_key}
app.get('/fetch/:project_key', async (req, res) => {
    const { project_key } = req.params;
    const updates = await fs.readJson(updatesFile);

    if (updates[project_key]) {
        const filePath = path.join(filesDirectory, updates[project_key].file);
        console.log(`FETCH '${project_key}': ${updates[project_key].hash} (${updates[project_key].file})`)
        res.sendFile(filePath);
    } else {
        console.error(`FETCH '${project_key}': 400`)
        res.status(400).send('project_key does not exist.');
    }
});

app.listen(PORT, '0.0.0.0', () => {
    if(REQUIRE_AUTH) {
        console.log(`Server is running on 0.0.0.0:${PORT} WITH AUTH`);
    } else {
        console.log(`Server is running on 0.0.0.0:${PORT}`);
    }
});
