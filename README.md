# ESP_IOT - Firmware Server

This project implements a basic HTTP API/Server which provides an interface for uploading firmware files ("commit"), checking for updates ("check"), and serving firmware files for OTA updates ("fetch"). The API allows for multiple firmware files for different projects ("project_key"), and supports easy integration with CI/CD systems, see [Project Coming Soon](https://gitlab.com/esp_iot)


> Note OTA updates of ESP32s require HTTP**S**. This project does not handle that -- I recommend running this server behind a reverse_proxy which handles that (Like [CaddyV2](https://caddyserver.com/))

It's built using Node.js because ChatGPT is good at that 😉

## Features

- **Upload a File**: Stores a file under a given `project_key` identified by a string `hash` (or another string identifier)
- **Check for Updates**: Returns the stored string `hash` for a given `project_key`. If it's different an update is available!
- **Serve a File**: Returns the file uploaded with a given `project_key`.
- **List all Updates**: Returns the internal JSON file with all available `project_key`, `hash`, and filenames.

## Getting Started

### Prerequisites

- Node.js (v14 or newer)
- npm
- Docker (optional for containerization)

### Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/esp_iot/firmware_server.git
```

```bash
cd firmware_server
```

### Running the Server from the command line

To build and run the server on your local machine:

1. Navigate into the project directory:

2. Install dependencies:

```bash
npm install
```

3. Start the server:

```bash
node server.js
```

### Running the Server using Docker (Gitlab Container Registry)

```bash
docker run -p 3000:3000 registry.gitlab.com/esp_iot/firmware_server
```

Run with OAUTH2.0 Authentication

```bash
docker run -p 4000:4000 -e SERVER_PORT=4000 -e REQUIRE_AUTH=true -e OAUTH_URI=https://auth.example.com/.well-known/openid-configuration registry.gitlab.com/esp_iot/firmware_server
```

### Build the Server using Docker
To build and run the server as a Docker container:

1. Build the Docker image:

```bash
docker build -t firmware_server .
```

2. Run the container:

```bash
docker run -p 3000:3000 firmware_server
```

You can change the ports being used with:
```bash
docker run -p 4000:4000 -e SERVER_PORT=4000 firmware_server
```

## Usage

### Pushing a file using shared/commit_file.sh
The commit_file.sh script facilitates uploading files to the server. Please see [shared/README.md](shared/README.md#commit_filesh) for more extensive usage information

#### Basic Usage

```bash
./commit_file.sh <project_key> <hash> <file_path>
```

```bash
./commit_file.sh -u https://fw.example.com -p 443 <project_key> <hash> <file_path>
```

```bash
./commit_file.sh -d <OAUTH_URI> -c <CLIENT_ID> -u <USERNAME> -P <PASSWORD> <project_key> <hash> <file_path>
```

## Configuration
Environment variables can be used to configure the server, including toggling authentication and setting the server URL and port. These can be specified in a .env file for local development or passed to Docker with the -e flag.

- `SERVER_PORT`: The server port. Default is `3000`
- `SERVER_URL`: The URL of the server. Default is `http://localhost`
- `REQUIRE_AUTH`: Require JWT authentication when commiting files to the server
- `OAUTH_URI`: The Discovery URL of your OAUTH2.0 provider (`.well-known/openid-configuration`)

## Contributing
We welcome contributions! Please open an issue or submit a pull request for any improvements or feature additions.